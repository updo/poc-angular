import { Component,
  Input,
  Output,
  ElementRef,
  ViewChild,
  Renderer,
  forwardRef,
  EventEmitter,
  OnInit } from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

const INLINE_EDIT_CONTROL_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => InlineEditComponent),
  multi: true
};

@Component({
  selector: 'app-inline-edit',
  templateUrl: './inline-edit.component.html',
  providers: [INLINE_EDIT_CONTROL_VALUE_ACCESSOR],
  styleUrls: ['./inline-edit.component.css']
})

export class InlineEditComponent implements ControlValueAccessor, OnInit {

  @ViewChild('inlineeditcontrol') inlineeditcontrol: ElementRef; // input DOM element
  @Input() label: string = '';  // Label value for input element
  @Input() type: string = 'text'; // The type of input element
  @Input() required: boolean = false; // Is input requried?
  @Input() disabled: boolean = false; // Is input disabled?
  @Output() inputUpdated = new EventEmitter(); //output emitter
  private _value: string = ''; // Private variable for input value
  private _preValue: string = ''; // The value before clicking to edit
  private _editing: boolean = false; // Is Component in edit mode?
  public onChange: any = Function.prototype; // Trascend the onChange event
  public onTouched: any = Function.prototype; // Trascend the onTouch event



  // Control Value Accessors for ngModel
  get value(): any {
    return this._value;
  }

  set value(v: any) {
    if (v !== this._value) {
      this._value = v;
      this.onChange(v);
    }
  }
  get prevalue(): any {
    return this._preValue;
  }

  set prevalue(prevalue: any) {
      this._preValue = prevalue;
  }
  get editing(): boolean {
    return this._editing;
  }

  set editing (editing: boolean) {
      this._editing = editing;
  }

  constructor(element: ElementRef, private _renderer: Renderer) {
  }

  // Required for ControlValueAccessor interface
  writeValue(value: any) {
    this._value = value;
  }

  // Required forControlValueAccessor interface
  public registerOnChange(fn: (_: any) => {}): void {
    this.onChange = fn;
  }

  // Required forControlValueAccessor interface
  public registerOnTouched(fn: () => {}): void {
    this.onTouched = fn;
  }

  keyHandler(event: KeyboardEvent){

    console.log(event.keyCode,this._preValue,this._value);

    if ( event.keyCode === 27) {
      this.cancelValue(event);
    }else
    if ( event.keyCode === 13 ){
      this.saveValue(event);
    }
  }

  // Do stuff when the input element loses focus
  onBlur($event: Event) {
    if ( !this._editing ) {
      return;
    }
      this.saveValue($event);

  }

  saveValue(event) {
    this._value = this._preValue;
    this.inputUpdated.emit(this._value);
    this._editing = false;
  }
  cancelValue(event){
    this.prevalue = this._value ;
    this._editing = false;
    console.log('je veux annuler l\'edition');
  }

  // Start the editting process for the input element
  edit(value) {
    if (this.disabled) {
      return;
    }

    this._preValue = value;
    this._editing = true;
    // Focus on the input element just as the editing begins
    setTimeout(_ => this._renderer.invokeElementMethod(this.inlineeditcontrol,
      'focus', []));
  }

  ngOnInit() {
  }
}
